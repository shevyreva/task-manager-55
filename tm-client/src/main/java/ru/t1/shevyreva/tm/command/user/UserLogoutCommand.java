package ru.t1.shevyreva.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.shevyreva.tm.dto.request.user.UserLogoutRequest;
import ru.t1.shevyreva.tm.enumerated.Role;

@Component
public class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    private final String DESCRIPTION = "User logout.";

    @NotNull
    private final String NAME = "logout";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        @NotNull final UserLogoutRequest request = new UserLogoutRequest();
        request.setToken(getToken());
        authEndpoint.logout(request);
        setToken(null);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
