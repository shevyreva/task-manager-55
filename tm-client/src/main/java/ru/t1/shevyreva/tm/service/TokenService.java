package ru.t1.shevyreva.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.shevyreva.tm.api.service.ITokenService;

@Getter
@Setter
@Service
public class TokenService implements ITokenService {

    @Nullable
    private String token;

}
