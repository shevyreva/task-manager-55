package ru.t1.shevyreva.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.shevyreva.tm.dto.request.system.ServerVersionRequest;
import ru.t1.shevyreva.tm.dto.response.system.ServerVersionResponse;

@Component
public class ApplicationVersionCommand extends AbstractSystemCommand {

    @NotNull
    private final String DESCRIPTION = "Show program version.";

    @NotNull
    private final String NAME = "version";

    @NotNull
    private final String ARGUMENT = "-v";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        @NotNull final ServerVersionRequest request = new ServerVersionRequest();
        @NotNull ServerVersionResponse response = systemEndpoint.getVersion(request);
        System.out.println(response.getVersion());
    }

}
