package ru.t1.shevyreva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.shevyreva.tm.api.component.ISaltProvider;

@Service
public interface IPropertyService extends ISaltProvider {

    @NotNull String getApplicationVersion();

    @NotNull String getAuthorName();

    @NotNull String getAuthorEmail();

    @NotNull Integer getServerPort();

}
