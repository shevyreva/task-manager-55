package ru.t1.shevyreva.tm.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.t1.shevyreva.tm.api.endpoint.*;

@Configuration
@ComponentScan("ru.t1.shevyreva.tm")
public class ClientConfiguration {

    @Bean
    @NotNull
    public ISystemEndpoint getSystemEndpoint() {
        return ISystemEndpoint.newInstance();
    }

    @Bean
    @NotNull
    public IDomainEndpoint getDomainEndpoint() {
        return IDomainEndpoint.newInstance();
    }

    @Bean
    @NotNull
    public IProjectEndpoint getProjectEndpoint() {
        return IProjectEndpoint.newInstance();
    }

    @Bean
    @NotNull
    public ITaskEndpoint getTaskEndpoint() {
        return ITaskEndpoint.newInstance();
    }

    @Bean
    @NotNull
    public IUserEndpoint getUserEndpoint() {
        return IUserEndpoint.newInstance();
    }

    @Bean
    @NotNull
    public IAuthEndpoint getAuthEndpoint() {
        return IAuthEndpoint.newInstance();
    }

}
