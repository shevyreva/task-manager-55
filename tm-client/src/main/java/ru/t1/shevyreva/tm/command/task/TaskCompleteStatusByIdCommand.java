package ru.t1.shevyreva.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.shevyreva.tm.dto.request.task.TaskChangeStatusByIdRequest;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.util.TerminalUtil;

@Component
public class TaskCompleteStatusByIdCommand extends AbstractTaskCommand {

    @NotNull
    private final String DESCRIPTION = "Complete task by Id.";

    @NotNull
    private final String NAME = "task-update-status-by-id";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("Enter Id:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(getToken(), id, Status.COMPLETED);
        taskEndpoint.changeTaskStatusById(request);
    }

}
