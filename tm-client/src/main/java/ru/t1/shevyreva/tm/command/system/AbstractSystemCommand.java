package ru.t1.shevyreva.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.shevyreva.tm.api.endpoint.ISystemEndpoint;
import ru.t1.shevyreva.tm.api.service.ICommandService;
import ru.t1.shevyreva.tm.api.service.IPropertyService;
import ru.t1.shevyreva.tm.command.AbstractCommand;
import ru.t1.shevyreva.tm.enumerated.Role;

@Component
public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    @Autowired
    public ISystemEndpoint systemEndpoint;

    @Nullable
    protected ICommandService getCommandService() {
        if (getServiceLocator() == null) return null;
        return getServiceLocator().getCommandService();
    }

    @Nullable
    public IPropertyService getPropertyService() {
        if (getServiceLocator() == null) return null;
        return getServiceLocator().getPropertyService();
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
