package ru.t1.shevyreva.tm.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.request.AbstractUserRequest;

public class DataLoadBinaryRequest extends AbstractUserRequest {

    public DataLoadBinaryRequest(@Nullable String token) {
        super(token);
    }

}
